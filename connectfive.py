# Benson Liu
# bliu13@ucsc.edu
#
# CMPS 5P, Spring 2014
# Assignment 5
#
# This game allows two players to play a game of Connect Five against each other. It
# is similar to Connect Four but the players must have five pieces instead to win.
# The game will have nine vertical columns and seven rows. The players alternate
# turns dropping in checkers and if the board is filled with no winners, it is a tie.
# There are 9 columns and 7 rows in this version.

__author__ = 'Ben'


board = [''] * 9
player_1 = 'X'
player_2 = 'O'


def read_user_input():
    """
    Function takes in user input.
    """
    avg_return = -1
    while True:
        raw_data = input('Which column do you want to drop your piece on? ')
        try:
            avg_return = int(raw_data)
        except ValueError:
            print('What you entered is not a number, choose a value between 0 and 8.\n')
            continue
        if avg_return < 0:
            print('Your value is below 0, choose a value between 0 and 8.\n')
            continue
        if avg_return > 8:
            print('Your value is above 40.0, choose a value between 0 and 8.\n')
            continue
        else:
            break
    return avg_return


def print_top_column():
    """
    Function prints the index of the column
    """
    top_column = '0 1 2 3 4 5 6 7 8'
    print(top_column)


def display_board():
    """
    Function prints the whole connect five board to display where
    each piece is placed at. The board is hardcoded for a set size
    and any additional elements in the strings will not be printed.
    Limits, Up to 7 rows and 9 columns
    """
    print_top_column()
    # Starting to print row by row, starting from the top.
    # Row at index 6 is the top row and row at index 0 is the
    # bottom row.
    row_index_offset = 1
    column_index_offset = 1
    for row in range(6, -1, -1):
        row_layout = ''
        for column in range(0, 9):
            if len(board[column]) >= (row + row_index_offset):
                # If the length of the string at our column is
                # greater than or equal to our row index.
                # In other words, if an element in string exists
                # in this row position, add in the element to
                # row_layout to print.
                row_layout += board[column][row]
            if len(board[column]) < (row + row_index_offset):
                # If the element in that column does not exist
                # in that row position, do not print a character
                # but in its place, an extra space.
                row_layout += ' '

            if column < (len(board) - column_index_offset):
                # If it is not last column, add in a space for
                # aesthetics.
                row_layout += ' '
        print(row_layout)


def get_piece(column, row):
    """
    This function checks to see if a piece at the given location exists.
    If it does it returns whatever is in that row and column, which is either
    an 'X' or 'O'. Else it will return 'Z'.

    'Z' is returned if the value at that particular column and row does not
    exist. When the value does not exist, it means the row list was not long
    enough and trying to access a location that is not there will yield an
    exception.
    """
    try:
        data = board[column][row]
    except IndexError:
        data = 'Z'
    return data


def win_detection(column_index):
    """
    This function checks for winning combinations right after a piece
    was placed into the board. The information this function needs
    is the column that the last piece was dropped on and which player
    it is. Returns 0 for no wins, or returns 1 for detecting win combination.

    The parameter column_index is the 'last piece' that was dropped into
    the board. The function will read what this last value was and then
    begins to search for the feasible combinations.

    If neither 'X' or 'O', 'Z' in this function means "unknown".
    """
    row_index_offset = 1
    row_index = len(board[column_index]) - row_index_offset
    latest_player_piece = 'Z'
    try:
        latest_player_piece = board[column_index][row_index]
    except IndexError:
        print('Error: No piece exist at column {0}'.format(column_index))
        exit(-1)

    if latest_player_piece is player_1:
        # Scanning the Top Left Diagonal and Bottom Right Diagonal
        #  Checking Top Left
        x_count = 1
        for offset in range(1, 5):
            board_piece = get_piece(column_index - offset, row_index + offset)
            if board_piece is 'Z':
                break
            elif board_piece is 'O':
                break
            elif board_piece is 'X':
                x_count += 1
        #  Checking Bottom Right
        for offset in range(1, 5):
            board_piece = get_piece(column_index + offset, row_index - offset)
            if board_piece is 'Z':
                break
            elif board_piece is 'O':
                break
            elif board_piece is 'X':
                x_count += 1
        if x_count >= 5:
            return 1

        # Scanning the Bottom Left Diagonal and Top Right Diagonal
        #  Checking Bottom Left
        x_count = 1
        for offset in range(1, 5):
            board_piece = get_piece(column_index - offset, row_index - offset)
            if board_piece is 'Z':
                break
            elif board_piece is 'O':
                break
            elif board_piece is 'X':
                x_count += 1
        #  Checking Top Right
        for offset in range(1, 5):
            board_piece = get_piece(column_index + offset, row_index + offset)
            if board_piece is 'Z':
                break
            elif board_piece is 'O':
                break
            elif board_piece is 'X':
                x_count += 1
        if x_count >= 5:
            return 1

        # Scanning Left and Right
        #  Checking Left
        x_count = 1
        for offset in range(1, 5):
            board_piece = get_piece(column_index - offset, row_index)
            if board_piece is 'Z':
                break
            elif board_piece is 'O':
                break
            elif board_piece is 'X':
                x_count += 1
        #  Checking Right
        for offset in range(1, 5):
            board_piece = get_piece(column_index + offset, row_index)
            if board_piece is 'Z':
                break
            elif board_piece is 'O':
                break
            elif board_piece is 'X':
                x_count += 1
        if x_count >= 5:
            return 1

        # Scanning Bottom, there should be no piece above this piece
        x_count = 1
        for offset in range(1, 5):
            board_piece = get_piece(column_index, row_index - offset)
            if board_piece is 'Z':
                break
            elif board_piece is 'O':
                break
            elif board_piece is 'X':
                x_count += 1
        if x_count >= 5:
            return 1

    if latest_player_piece is player_2:
        # Scanning the Top Left Diagonal and Bottom Right Diagonal
        #  Checking Top Left
        o_count = 1
        for offset in range(1, 5):
            board_piece = get_piece(column_index - offset, row_index + offset)
            if board_piece is 'Z':
                break
            elif board_piece is player_2:
                o_count += 1
            elif board_piece is player_1:
                break
        #  Checking Bottom Right
        for offset in range(1, 5):
            board_piece = get_piece(column_index + offset, row_index - offset)
            if board_piece is 'Z':
                break
            elif board_piece is player_2:
                o_count += 1
            elif board_piece is player_1:
                break
        if o_count >= 5:
            return 1

        # Scanning the Bottom Left Diagonal and Top Right Diagonal
        #  Checking Bottom Left
        o_count = 1
        for offset in range(1, 5):
            board_piece = get_piece(column_index - offset, row_index - offset)
            if board_piece is 'Z':
                break
            elif board_piece is player_2:
                o_count += 1
            elif board_piece is player_1:
                break
        #  Checking Top Right
        for offset in range(1, 5):
            board_piece = get_piece(column_index + offset, row_index + offset)
            if board_piece is 'Z':
                break
            elif board_piece is player_2:
                o_count += 1
            elif board_piece is player_1:
                break
        if o_count >= 5:
            return 1

        # Scanning Left and Right
        #  Checking Left
        o_count = 1
        for offset in range(1, 5):
            board_piece = get_piece(column_index - offset, row_index)
            if board_piece is 'Z':
                break
            elif board_piece is player_2:
                o_count += 1
            elif board_piece is player_1:
                break
        #  Checking Right
        for offset in range(1, 5):
            board_piece = get_piece(column_index + offset, row_index)
            if board_piece is 'Z':
                break
            elif board_piece is player_2:
                o_count += 1
            elif board_piece is player_1:
                break
        if o_count >= 5:
            return 1

        # Scanning Bottom, there should be no piece above this piece
        o_count = 1
        for offset in range(1, 5):
            board_piece = get_piece(column_index, row_index - offset)
            if board_piece is 'Z':
                break
            elif board_piece is player_2:
                o_count += 1
            elif board_piece is player_1:
                break
        if o_count >= 5:
            return 1

    # Absolutely no winning combinations detected
    return 0


def drop_piece(player_piece, column):
    """
    Function drops the specified piece at the specified column.
    Each element in the list represents the column in the board
    and each element's index represents which row it is at.
    In each element of the list, index 0 in that element represents
    a piece at the bottom row while at index 6, it represents a piece
    that would be at the top row if there exist one there.
    board[column][0] = bottom row in string
    board[column][6] = highest row in string

    Function returns 0 on success and returns 1 on failure appending string.
    """
    global board

    if len(board[column]) > 6:
        print('Error: Too many pieces on one column.')
        return 1
    board[column] += player_piece
    return 0


if __name__ == '__main__':
    print('This is a game called Connect Five and it requires two people to play.')
    print('Player 1 will always be X and make the first move. Player 2 will always be O.\n')
    display_board()

    # If player = 0, then it is player 1's turn. If player = 1, then it is player 2's turn.
    player = 0
    while True:
        if player is 0:
            print('Turn: Player 1')
            user_input_column = read_user_input()
            drop_status = drop_piece(player_1, user_input_column)
            if drop_status is 1:
                print('Insert into another column.\n')
                continue

            display_board()
            win_check = win_detection(user_input_column)
            if win_check is 1:
                print('Player 1 has won.')
                break
            player = 1

        if player is 1:
            print('Turn: Player 2')
            user_input_column = read_user_input()
            drop_status = drop_piece(player_2, user_input_column)
            if drop_status is 1:
                print('Insert into another column.\n')
                continue

            display_board()
            win_check = win_detection(user_input_column)
            if win_check is 2:
                print('Player 2 has won.')
                break
            player = 0